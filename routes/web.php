<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Facade\FlareClient\View;

// Get Method

// Menampilkan halaman home
Route::get('/', 'HomeController@index');
// Menampilkan Halaman Register
Route::get('/register', 'AuthController@index');

// Menampilkan Halaman Table
Route::get('/table', function() {
    return view('table');
});

// Menampilkan Halaman Data Tables
Route::get('/data-tables', function() {
    return view('data-tables');
});

// Post Method
Route::post('/welcome', 'AuthController@welcome');