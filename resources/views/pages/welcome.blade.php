@extends('layouts.admin.master')

@section('title')
    Welcome
@endsection

@section('content')
    <h1>Selamat Datang, {{$first_name}} {{$last_name}}</h1>

    <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
@endsection